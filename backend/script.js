$( function() {
var cache = {};
$( "#vvs_start" ).autocomplete({
  minLength: 2,
  source: function( request, response ) {
  var term = request.term;
  if ( term in cache ) {
    response( cache[ term ] );
    return;
  }

  $.getJSON( "https://efa-api.asw.io/api/v1/station?search="+term, request, function( data, status, xhr ) {
    var list = {};
    for (var i = 0; i < data.length; i++) {
      var object = data[i];
      if($.isNumeric( object.stationId ) === true)
      {
        var newObject = new Object();
        newObject.id = object.stationId;
        newObject.label = object.fullName;
        newObject.value = object.fullName;
        list[i] = newObject;
      }
    }
    cache[ term ] = list;
    response( list );
  });
  }
  } );
} );

$('#vvs__edit').click(function() {
  var vvs_start = $("#vvs_start").val();
  var vvs_bus = $("#vvs_bus").is(':checked');
  var vvs_ubahn = $("#vvs_ubahn").is(':checked');
  var vvs_sbahn = $("#vvs_sbahn").is(':checked');
  var vvs_zuege = $("#vvs_zuege").is(':checked');

  $.ajax({
  url: "https://efa-api.asw.io/api/v1/station?search="+vvs_start,
  dataType: 'json',
  async: true,
  success: function(data) {
      var found = false;
      for (var i = 0; i < data.length; i++) {
        var object = data[i];
        if($.isNumeric( object.stationId ) === true && object.fullName === vvs_start)
        {
          $.post('setConfigValueAjax.php', {'key': 'vvs_startId', 'value': object.stationId});
          $.post('setConfigValueAjax.php', {'key': 'vvs_startName', 'value': object.fullName});
          $.post('setConfigValueAjax.php', {'key': 'vvs_bus', 'value': vvs_bus });
          $.post('setConfigValueAjax.php', {'key': 'vvs_ubahn', 'value': vvs_ubahn});
          $.post('setConfigValueAjax.php', {'key': 'vvs_sbahn', 'value': vvs_sbahn});
            $.post('setConfigValueAjax.php', {'key': 'vvs_zuege', 'value': vvs_zuege});


          $("#vvs_ok").text("Daten erfolgreich gespeichert");
          $("#vvs_error").text("");

          $('#ok').show(30, function() {
            $(this).hide('slow');
          });
          found = true;
        }
      }

      if(found === false) {
        $("#vvs_ok").text("");
        $("#vvs_error").text("Fehler, Haltestelle nicht gefunden");
        $('#error').show(30, function() {
          $(this).hide('slow');
        });
      }
    }
  });
});
